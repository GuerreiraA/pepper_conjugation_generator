#!/usr/bin/env python
# coding: utf-8

"""
Version 0.0.1
"""
import argparse
import os

vowel = ('a', 'e', 'i', 'o', 'u', 'y')


def check_third_group(verb):
    f = open('verb_third_group.txt', 'r')
    all_verbs = f.read()
    return 1 if verb in all_verbs else 0
    f.close()


def conjugate(verb, person):
    if verb[-2:] == 'er' and verb != 'aller' and verb != 'envoyer':
        if person == 'i':
            return(verb[:-1])
        elif person == 'you':
            return(('[{0} {1}ais]').format(verb[:-1] + 's', verb))
        elif person == 'you2':
            return(('[{0} {1}iez]').format(verb[:-1] + 'z', verb))
    elif verb[-2:] == 'ir' and check_third_group(verb) == 0:
        if person == 'i':
            return(('{}').format(verb[:-1] + 's'))
        elif person == 'you':
            return(('[{0} {1}ais]').format(verb[:-1] + 's', verb))
        elif person == 'you2':
            return(('[{0} {1}iez]').format(verb[:-1] + 'ssez', verb))
    else:
        print(verb + ' needs your attention.')


def subject(verb, form, person):
    if person == 'i':
        if form == 'pos':
            if verb[0] in vowel:
                return('j\'')
            else:
                return('je ')
        elif form == 'neg':
            if verb[0].lower() in vowel:
                return('[je j\'] {n\'} ')
            else:
                return('je {ne} ')
    elif person == 'you':
        if form == 'pos':
            if verb[0] in vowel:
                return('[tu t\'] ')
            else:
                return('tu ')
        elif form == 'neg':
            if verb[0] in vowel:
                return('[tu t\'] {n\'} ')
            else:
                return('tu {ne} ')
    elif person == 'you2':
        if form == 'pos':
            return('vous ')
        elif form == 'neg':
            if verb[0] in vowel:
                return('vous {n\'} ')
            else:
                return('vous {ne} ')


def add_conjugation_frf(verb, lexicon):
    lexicon.write(('{0}{1}{0}\n'
                   '{0} {2}\n'
                   '{3}pos_i_{2}) [\"{5}{11}\"]\n'
                   '{3}pos_you_{2}) [\"{6}{12}\"]\n'
                   '{3}pos_you2_{2}) [\"{7}{13}\"]\n'
                   '{3}pos_you_all_{2}) [~vb_pos_you_{2} ~vb_pos_you2_{2}]\n'
                   '{3}neg_i_{2}) [\"{8}{11} {14}\"]\n'
                   '{3}neg_you_{2}) [\"{9}{12} {14}\"]\n'
                   '{3}neg_you2_{2}) [\"{10}{13} {14}\"]\n'
                   '{3}neg_you_all_{2}) [~vb_neg_you_{2} ~vb_neg_you2_{2}]\n'
                   '{3}int_you_{2}) [\"{12} tu\"]\n'
                   '{3}int_you2_{2}) [\"{13} vous\"]\n'
                   '{3}int_you_all_{2}) [~vb_int_you_{2} ~vb_int_you2_{2}]\n'
                   '{3}int_neg_you_{2}) [\"{4} {12} tu {14}\"]\n'
                   '{3}int_neg_you2_{2}) [\"{4} {13} vous {14}\"]\n'
                   '{3}int_neg_you_all_{2}) [~vb_int_neg_you_{2}'
                   ' ~vb_int_neg_you2_{2}]\n'
                   '{3}i_{2}) [~vb_pos_i_{2} ~vb_neg_i_{2}]\n'
                   '{3}you_{2}) [~vb_pos_you_all_{2} ~vb_neg_you_all_{2}]\n'
                   '{3}int_{2}) [~vb_int_you_all_{2}'
                   ' ~vb_int_neg_you_all_{2}]\n'
                   '{3}ask_{2}) [\"est-ce que ~vb_you_{2}\"'
                   ' ~vb_you_{2} ~vb_int_{2}]\n'
                   ).format('#', '-' * 77, verb, 'concept:(vb_', '{ne}',
                            subject(verb, 'pos', 'i'),
                            subject(verb, 'pos', 'you'),
                            subject(verb, 'pos', 'you2'),
                            subject(verb, 'neg', 'i'),
                            subject(verb, 'neg', 'you'),
                            subject(verb, 'neg', 'you2'),
                            conjugate(verb, 'i'),
                            conjugate(verb, 'you'),
                            conjugate(verb, 'you2'),
                            '[pas plus]',
                            ))
# ajouter la forme int_neg


def grooming_list_verbs(args):
    args.verb = list(set(args.verb))
    args.verb.sort()
    try:
        f = open(args.file, 'r')
    except:
        pass
    else:
        lexicon = f.read()
        for verb in args.verb:
            if verb in lexicon:
                args.verb = [i for i in args.verb if i != verb]
        f.close()


def backup_verb(verb):
    f = open(args.backup, 'a')
    f.write(verb)
    f.close()


def add_header(f, args):
    f.write(('{0}{1}{0}\n'
             '{0} Version 0.0.1\n'
             '{0}{1}{0}\n'
             'topic: ~{2} ()\n'
             'language: {3}\n'
             ).format('#', '=' * 77, args.file[:-4], args.file[-7:-4]))


def add_verb_lexicon(args):
    try:
        size = os.path.getsize(args.file)
    except:
        size = 0
    f = open(args.file, 'a')
    if size == 0:
        add_header(f, args)
    if 'frf' in args.file:
        for verb in args.verb:
            add_conjugation_frf(verb.lower(), f)
            backup_verb(verb + ' ')
    f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-v', '--verb', nargs='+',
                        help='Type a serial of infinitive verbs',
                        required=True)
    parser.add_argument('-f', '--file', type=str,
                        default='lexicon_verbs_frf.top',
                        help='Name of file where add conjugate verbs')
    parser.add_argument('-b', '--backup', type=str,
                        default='added_verbs_frf.txt',
                        help='File which contains the list of added verbs')
    args = parser.parse_args()
    grooming_list_verbs(args)
    add_verb_lexicon(args)
